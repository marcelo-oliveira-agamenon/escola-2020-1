package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 tera 16
	 * anos. Caso de teste 
	 * # | entrada 				 | saida esperada 
	 * 1 | aluno nascido em 2003 |	 * 16
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		DateHelper dh = new DateHelper();
		Aluno aluno = new Aluno.AlunoBuilder()
				.matricula(2)
				.nome("Teste")
				.situacaoAluno(SituacaoAluno.ATIVO)
				.anoNascimento(2003)
				.criarAluno();
		AlunoDAO a = new AlunoDAO();
		a.salvar(aluno);
		AlunoBO bo = new AlunoBO(a, dh);
		Integer n = bo.calcularIdade(aluno.getMatricula());
		Integer expected = 16;
		Assert.assertEquals(expected, n);
	}

	/**
	 * Verificar se alunos ativos sao atualizados.
	 */
	public void testarAtualizacaoAlunosAtivos() {
		DateHelper dh = new DateHelper();
		AlunoDAO a = new AlunoDAO();
		Aluno aluno = a.encontrarPorMatricula(1);
		AlunoBO bo = new AlunoBO(a, dh);
		bo.atualizar(aluno);
	}

}
