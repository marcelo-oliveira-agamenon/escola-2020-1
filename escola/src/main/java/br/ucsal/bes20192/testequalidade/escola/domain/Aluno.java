package br.ucsal.bes20192.testequalidade.escola.domain;

public class Aluno {

	private Integer matricula;

	private String nome;

	private SituacaoAluno situacao;

	private Integer anoNascimento;

	
	
	public Aluno(Integer matricula, String nome, SituacaoAluno situacao, Integer anoNascimento) {
		super();
		this.matricula = matricula;
		this.nome = nome;
		this.situacao = situacao;
		this.anoNascimento = anoNascimento;
	}

	public Aluno() {
		super();
	}
	
	public static class AlunoBuilder{
		private Integer matricula;

		private String nome;

		private SituacaoAluno situacao;

		private Integer anoNascimento;

		public AlunoBuilder() {
		}
		
		public AlunoBuilder matricula(Integer matricula) {
			this.matricula = matricula;
			return this;
		}
		
		public AlunoBuilder nome(String nome) {
			this.nome = nome;
			return this;
		}
		
		public AlunoBuilder situacaoAluno(SituacaoAluno situacao) {
			this.situacao = situacao;
			return this;
		}
		
		public AlunoBuilder anoNascimento(Integer anoNascimento) {
			this.anoNascimento = anoNascimento;
			return this;
		}
		
		public Aluno alunoBuilder() {
			return new Aluno(matricula, nome, situacao, anoNascimento);
		}

		public Aluno criarAluno() {
			return new Aluno(matricula, nome, situacao, anoNascimento);
		}
		
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public SituacaoAluno getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAluno situacao) {
		this.situacao = situacao;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((anoNascimento == null) ? 0 : anoNascimento.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (anoNascimento == null) {
			if (other.anoNascimento != null)
				return false;
		} else if (!anoNascimento.equals(other.anoNascimento))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (situacao != other.situacao)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", situacao=" + situacao + ", anoNascimento="
				+ anoNascimento + "]";
	}

}