package br.ucsal.bes20192.testequalidade.escola.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AbstractDAO {

	private static final String USER = "root";
	private static final String PASSWORD = "";
	private static final String STRING_CONNECTION = "jdbc:mysql://localhost:3306/atividade05?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

	private Connection connection = null;

	protected Connection getConnection() {
		if (connection == null) {
			conectar();
		}
		return connection;
	}

	private void conectar() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(STRING_CONNECTION, USER, PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}
